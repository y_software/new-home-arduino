#include "new_home_arduino.hpp"
#include <ESP8266WiFi.h>

class ExampleMethod : public NewHomeMethod {
  public:
    bool can_handle(String method_name) {
      Serial.println(method_name);

      return method_name.equals("example");
    }

    JSONVar handle_request(JSONVar request) {
      JSONVar response;

      response["code"] = 0;
      response["message"] = "It works!";

      return response;
    }
};

NewHomeMethod *methods[] = {new ExampleMethod()};
WiFiServer server(80);
NewHomeArduino application(server, methods, 1);

void setup() {
  application.setup();
}

void loop() {
  application.loop();
}
