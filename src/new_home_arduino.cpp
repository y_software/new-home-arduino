#include <FS.h>
#include "new_home_arduino.hpp"

NewHomeArduino::NewHomeArduino(WiFiServer server, NewHomeMethod *methods[], size_t method_count):
  server(server), methods(methods), method_count(method_count) {
}

void NewHomeArduino::setup() {
  Serial.begin(115200);

  NewHomeWifiManager wifi_manager;
  NewHomeWifiInfo wifi_info = wifi_manager.get_wifi_info();

  WiFi.begin(wifi_info.ssid, wifi_info.passphrase);

  Serial.println("Connecting to WiFi:");
  Serial.print(wifi_info.ssid);
  Serial.print(" ");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(200);
  }

  Serial.println(" connected.");
  delay(10);

  Serial.print("Getting IP address ");

  while (!WiFi.localIP().isSet()) {
    Serial.print(".");
    delay(100);
  }

  Serial.println(" done");
  Serial.println("");

  Serial.print("My IP is ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting server on Port 80");
  server.begin();
  Serial.println("Server listening");
}

void NewHomeArduino::loop() {
  WiFiClient client = server.available();

  if (!client.connected()) {
    delay(10);

    return;
  }

  String chars;
  delay(10);

  while (client.available()) {
    chars += char(client.read());
  }

  JSONVar request = JSON.parse(chars);
  JSONVar response;
  response["code"] = 1;
  response["message"] = "Could not parse JSON";


  if (JSON.typeof(request) == "undefined") {
    client.println(JSON.stringify(response));
    client.stop();

    return;
  }

  if (!request.hasOwnProperty("method_name")) {
    response["message"] = "Missing method or arguments key";
    client.println(JSON.stringify(response));
    client.stop();

    return;
  }

  response = this->handle_request(request);

  client.println(JSON.stringify(response));
  client.stop();
}

JSONVar NewHomeArduino::handle_request(JSONVar request) {
  String method_name = String((const char *) request["method_name"]);
  JSONVar response;

  response["message"] = "Method not found!";
  response["code"] = 1;

  if (this->methods == nullptr) {
    return response;
  }


  for (int index = 0; index < this->method_count; index++) {
    NewHomeMethod *method = this->methods[index];

    if (method->can_handle(method_name)) {
      response = method->handle_request(request);

      break;
    }
  }

  return response;
}


NewHomeWifiInfo NewHomeWifiManager::get_wifi_info() {
  SPIFFS.begin();
  this->initialize();
  this->update();

  return this->read_info_from_eep();
}

void NewHomeWifiManager::initialize() {
  Serial.println("Initialize from Serial");

  while (!this->is_initialized()) {
    this->read_info_from_serial();
  }

  Serial.println("Initialized!");
}

void NewHomeWifiManager::update() {
  Serial.println("Waiting for Serial update request");

  if (!this->is_new_info()) {
    Serial.println("No Serial command received");

    return;
  }

  Serial.println("Updating!");

  this->read_info_from_serial();

  Serial.println("Done updating");
}

bool NewHomeWifiManager::is_initialized() {
  return SPIFFS.exists("/wifi.txt");
}

void NewHomeWifiManager::read_info_from_serial() {
  Serial.setTimeout(300000);
  Serial.println("Enter SSID: ");
  String ssid = Serial.readStringUntil('\n');
  Serial.setTimeout(1000);

  if (ssid.length() > SSID_LENGTH) {
    Serial.println("SSID is too long. Aborting!");

    return;
  }

  Serial.setTimeout(300000);
  Serial.println("Enter Passphrase: ");
  String passphrase = Serial.readStringUntil('\n');
  Serial.setTimeout(1000);

  if (passphrase.length() > PASSPHRASE_LENGTH) {
    Serial.println("Passphrase is too long. Aborting!");

    return;
  }

  ssid.trim();
  passphrase.trim();

  NewHomeWifiInfo info = NewHomeWifiInfo{ssid, passphrase};

  this->write_wifi_info(info);
}

bool NewHomeWifiManager::is_new_info() {
  Serial.setTimeout(WAIT_UPDATE_WIFI_CREDS_MS);
  String init_string = Serial.readStringUntil('\n');
  Serial.setTimeout(1000);

  return init_string.startsWith("WIFI");
}

NewHomeWifiInfo NewHomeWifiManager::read_info_from_eep() {
  File file = SPIFFS.open("/wifi.txt", "r");
  String ssid = file.readStringUntil('\n');
  String passphrase = file.readStringUntil('\n');
  Serial.print("Got: ");
  Serial.print(ssid);
  Serial.print(" & ");
  Serial.println(passphrase);

  ssid.trim();
  passphrase.trim();

  return NewHomeWifiInfo{ssid, passphrase};
}

void NewHomeWifiManager::write_wifi_info(NewHomeWifiInfo info) {
  File file = SPIFFS.open("/wifi.txt", "w");
  file.println(info.ssid);
  file.println(info.passphrase);
  file.close();
}
