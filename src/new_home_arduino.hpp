#ifndef __NEW_HOME__NEW_HOME_ARDUINO_HPP__
#define __NEW_HOME__NEW_HOME_ARDUINO_HPP__

#define SSID_LENGTH 32
#define PASSPHRASE_LENGTH 64
#define WAIT_UPDATE_WIFI_CREDS_MS 500

#include <Arduino_JSON.h>
#include <ESP8266WiFi.h>

class NewHomeMethod {
  public:
    /**
       Checks if the method can handle the given request

       @param method_name - the name of the method
    */
    virtual bool can_handle(String method_name);

    /**
       @param request - The received request with the method name and the arguments

       @return A response json which has to contain at least a `code` element which has to be an integer (0 = success, >= 1 = error)
    */
    virtual JSONVar handle_request(JSONVar request);
};

class NewHomeArduino {
  protected:
    WiFiServer server;

    uint16_t port;

    NewHomeMethod **methods;

    size_t method_count;

  public:
    /**
       @param server - The server on which the application will listen
       @param methods - All the methods that should be callable by the outside
       @param method_count - The count of the methods
    */
    NewHomeArduino(WiFiServer server, NewHomeMethod *methods[], size_t method_count);

    /**
       Sets up WiFi connection and WiFi server
    */
    void setup();

    /**
       Accepts a client connection and handles it
    */
    void loop();

  protected:
    JSONVar handle_request(JSONVar request);
};

struct NewHomeWifiInfo {
  String ssid;
  String passphrase;
};

class NewHomeWifiManager {
  public:
    /**
       Gets wifi credentials from built in EEPROM if set

       If the EEPROM has no initialized data, it waits for data to come in via Serial
       - Line 1: SSID
       - Line 2: Passphrase

       If the EEPROM has WiFi data in it, the call will wait for 500ms and then check if a Serial input is avilable
       It has to start with a line containing "WIFI". Then the new credentials are expected to be given in the same format as the initial setup
    */
    NewHomeWifiInfo get_wifi_info();

  protected:
    /**
    *  Initializes the WiFi data from Serial
    */
    void initialize();

    /**
       Updates the WiFi data from Serial
    */
    void update();

    /**
       Reads the initialized byte from the EEPROM (EEP_WIFI_INITIALIZED)
    */
    bool is_initialized();

    /**
       Reads WiFi data from a serial input
    */
    void read_info_from_serial();

    /**
       Checks if the command for setting new WiFi info was sent to the Serial connection
    */
    bool is_new_info();

    /**
       Reads the WiFi data from the internal EEPROM (EEP_WIFI_SSID and EEP_WIFI_PASSPRHASE)
    */
    NewHomeWifiInfo read_info_from_eep();

    /**
       Writes the given info to the EEPROM and sets the initialized flag to true
    */
    void write_wifi_info(NewHomeWifiInfo info);
};

#endif
