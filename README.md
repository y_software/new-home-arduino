# New Home Application Framework (for Arduino)

This is the New Home Application framework for Arduinos

## Usage

The implementation is as simple as including the library and implementing the the `NewHomeMethod` class

The application will be loaded with an `#include <new_home_arduino.hpp>`. You can create a method with the help of the `NewHomeMethod` class.
To make your method available to the framework, you need to pass an array of methods as argument to a `NewHomeArduino` instance. 
Besides of the array it requires a WiFiServer (from the ESP8266Wifi.h library) as the first argument and the length of the array as the third and last argument.

For running the library you will have to call the `NewHomeArduino::setup()` and `NewHomeArduino::loop()` methods in the appropriate arduino functions.
The first time setup will request a WiFi username and password from a Serial, which will be stored when entered.
You can change the WiFi info by passing the keyword "WIFI" to the device during the first 500ms of startup of the device (after that you may have to restart the device to apply your changes.

For an example of how to implement the library you can take a look into the example/example.ino. It implements the library as its meant to be, and creates an example method.

### Including the library

- Download the project as ZIP file
- Open your Arduino IDE
- Add the library at `Sketch | Include Library | Add .ZIP Library`
- Done

### Implementation note

As well as for the Raspberry/Rust version of the framework you need to provide some methods for the New Home UI.
You can read which any why [here](https://gitlab.com/y_software/new-home-core#required-methods).

Note 2: when flashing the application, you have to add an SPIFFS which is used to store the WIFI Credentials

